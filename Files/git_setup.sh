#!/bin/bash

rm -rf /home/user/workspace/techberry-go
rm -rf /home/user/workspace/go-module/*

echo "Input git user name:"
read git_user
echo "Input git password:"
read git_password
echo "Input git email:"
read git_email

git config --global user.name "$git_user"
git config --global user.email "$git_password"
git clone --branch 'develop' --single-branch https://$git_user:$git_password@bitbucket.beebuddy.net/scm/gom_template/techberry-go-common.git
mv techberry-go-common common
mkdir techberry-go
mv common techberry-go/.
git clone --branch 'develop' --single-branch https://$git_user:$git_password@bitbucket.beebuddy.net/scm/training/hello-plugin.git go-module/micronode
