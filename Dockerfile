FROM fullaxx/ubuntu-desktop

#install vscode, git, jq 
RUN sudo apt-get update
RUN sudo apt-get install -y gpg
RUN echo "deb [arch=amd64] http://packages.microsoft.com/repos/vscode stable main" >> /etc/apt/sources.list
RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
RUN sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
RUN sudo apt-get update
RUN sudo apt-get install -y libasound2-dev code git jq
#RUN sudo apt-get install -y libtool-bin

# install golang
WORKDIR /tmp
RUN wget https://golang.org/dl/go1.16.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.16.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin

# install zeroMQ
#WORKDIR /tmp
#RUN sudo apt-get install -y libtool pkg-config build-essential autoconf automake 
#RUN wget https://download.libsodium.org/libsodium/releases/libsodium-1.0.18-stable.tar.gz
#RUN tar -zxvf libsodium-1.0.18-stable.tar.gz
#WORKDIR /tmp/libsodium-stable
#RUN sudo ./autogen.sh
#RUN sudo ./configure && make check
#RUN sudo make install
#RUN sudo ldconfig

#WORKDIR /tmp
#RUN wget http://download.zeromq.org/zeromq-4.1.4.tar.gz
#RUN tar -xvf zeromq-4.1.4.tar.gz
#WORKDIR /tmp/zeromq-4.1.4
#RUN sudo ./autogen.sh
#RUN sudo ./configure && make check
#RUN sudo make install
#RUN sudo ldconfig

# # install shell script to clone repo per user
# RUN mkdir -p /home/user/workspace/go-module
# RUN mkdir -p /home/user/vscode
# #ADD Files/git_setup.sh /home/user/workspace/git_setup.sh
# ADD Files/tb_git_setup /usr/local/bin/tb_git_setup
# #RUN chmod 711 /home/user/workspace/git_setup.sh
# ADD Files/tb_vscode /usr/local/bin/tb_vscode
# RUN chmod 511 /usr/local/bin/tb_git_setup /usr/local/bin/tb_git_setup

# install vscode plugin
ADD Files/Go-0.22.1_vsixhub.com.vsix /tmp/Go-0.22.1_vsixhub.com.vsix
RUN code --user-data-dir '/home/user/vscode' --install-extension /tmp/Go-0.22.1_vsixhub.com.vsix

# install Postman and create destok link
ADD Files/Postman-linux-x64-8.0.6.tar.gz /opt/stack/.
ADD Files/Postman.desktop /root/.local/share/applications/Postman.desktop 

WORKDIR /home/user/workspace/
